#!/bin/bash

ec --endpoints=$ENDPOINTS put foo bar
ec --endpoints=$ENDPOINTS put foo bat
ec --endpoints=$ENDPOINTS put foo baz

ec --endpoints=$ENDPOINTS get foo

ec --endpoints=$ENDPOINTS member list --write-out=table

# It outputs the status of the nodes in the cluster
ec --endpoints=$ENDPOINTS endpoint status --write-out=table

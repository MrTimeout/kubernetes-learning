#!/bin/bash

CLIENT_PORT=2379
PEER_PORT=2380

rm -rf /tmp/etcd
test $(docker network ls | grep -c etcd) -eq 0 && docker network create etcd-network --driver bridge --gateway 172.18.0.1 --subnet 172.18.0.0/16

gateway="$(docker network inspect etcd-network --format "{{json .IPAM.Config}}" | jq -r .[0].Gateway)"

# It's better to use redirection <<< $gateway, but neovim don't get syntax correctly.
prefix="$(echo $gateway | cut -d '.' -f 1,2,3)."
first=$(echo $gateway | cut -d '.' -f4)

ALPINE_VERSION=3.16 ETCD_VERSION=3.5.4; docker image build --no-cache --tag myetcd:latest --tag myetcd:0.1-SNAPSHOT -<<EOF
ARG ALPINE_VERSION=3.16
FROM alpine:$ALPINE_VERSION

ARG ETCD_VERSION

RUN apk add --no-cache --update bash jq && sed -i 's/\(.*\)\/bin\/ash/\1\/bin\/bash/g' /etc/passwd && \
  echo alias ec=etcdctl > ~/.bashrc && \
  wget https://github.com/etcd-io/etcd/releases/download/v${ETCD_VERSION}/etcd-v${ETCD_VERSION}-linux-amd64.tar.gz -O/tmp/etcd.tar.gz && \
  tar -xvzf /tmp/etcd.tar.gz -C /tmp && rm /tmp/etcd.tar.gz && mv /tmp/*/etcd* /usr/local/bin/

SHELL ["/bin/bash", "-c"]

EXPOSE ${CLIENT_PORT} ${PEER_PORT} 
EOF

set cluster_nodes
for n in $(seq $((first + 1)) $((first + 3))); do
  cluster_nodes+="etcd${n}=http://${prefix}${n}:${PEER_PORT},"
done

cluster_nodes=${cluster_nodes%,}

for n in $(seq $((first + 1)) $((first + 3))); do
  ip=${prefix}${n}
  echo "Running node etcd${n} with the ip $ip"

  mkdir --parent /tmp/etcd/data${n}

  docker container run --name etcd${n} --detach --rm \
    --network etcd-network --ip $ip \
    --mount type=bind,source=/tmp/etcd/data${n},destination=/etcd/data myetcd:0.1-SNAPSHOT \
    /usr/local/bin/etcd \
    --name etcd${n} \
    --data-dir /etcd/data \
    --initial-advertise-peer-urls http://${ip}:${PEER_PORT} \
    --listen-peer-urls http://${ip}:${PEER_PORT} \
    --advertise-client-urls http://${ip}:${CLIENT_PORT} \
    --listen-client-urls http://${ip}:${CLIENT_PORT} \
    --initial-cluster $cluster_nodes \
    --initial-cluster-state new \
    --initial-cluster-token mytoken \
    --log-level info \
    --log-outputs stderr \
    --logger zap
done

# etcdctl --endpoints=$cluster_nodes
docker container run --name etcd-client -it --network etcd-network \
  -e ETCDCTL_API=3 \
  -e ENDPOINTS="$(echo $cluster_nodes | sed 's/etcd[0-9]=//g')" myetcd:0.1-SNAPSHOT /bin/bash

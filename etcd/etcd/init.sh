#!/bin/bash

ETCD_VERSION=3.5.4

docker container rm -f etcd-simplest-server || true && docker container run --name etcd-simplest-server -d -e ETCDCTL_API=3 alpine:3.16 /bin/sh -c "
  wget https://github.com/etcd-io/etcd/releases/download/v${ETCD_VERSION}/etcd-v${ETCD_VERSION}-linux-amd64.tar.gz -O/tmp/etcd.tar.gz && \
  tar -xvzf /tmp/etcd.tar.gz -C /tmp && rm /tmp/etcd.tar.gz && mv /tmp/*/etcd* /usr/local/bin/ && /usr/local/bin/etcd
  "

echo "docker container exec -it etcd-simplest-server /bin/sh"

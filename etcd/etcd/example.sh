#!/bin/bash

alias ec=etcdctl

ec put foo bar
ec put foo baz
ec put foo bat
ec put foo baaaar

# Check that the mod_revision must be 5 and create_revision is 2...
ec get foo --write-out=json

# Check that you have jq installed
for x in $(seq 1 $(ec get foo --write-out=json | jq .kvs[0].mod_revision)); do ec get foo --rev=$x --print-value-only; done

# This will return the number of keys which were removed
ec del foo

ec get foo --rev=2 --print-value-only

ec put pod/first first
ec put pod/second second
ec put pod/third third
ec put pod/fourth fourth

ec get --prefix pod/ --print-value-only

# We can also watch for changes in one prefix
# ec watch --prefix pod/
#
# We can watch changes from a revision until now
# ec watch --prefix pod/ --rev=2

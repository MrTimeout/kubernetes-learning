#!/bin/bash

# We generate the root CA private key
openssl genpkey -algorithm RSA -out root-ca.pem -pkeyopt rsa_keygen_bits:4096 -pkeyopt rsa_keygen_pubexp:65537 -quiet

# Create the CSR from the CA private key
openssl req -new -key root-ca.pem -out root-ca.csr -sha256 -subj "/CN=my home"

# Self-sign the root cert
openssl x509 -req -days 365 -in root-ca.csr -signkey root-ca.pem -sha256 -out root-ca.crt

# Create the server private key
openssl genpkey -algorithm RSA -out server.pem -pkeyopt rsa_keygen_bits:4096 -pkeyopt rsa_keygen_pubexp:65537 -quiet

# Create the CSR for this server private key
openssl req -new -key server.pem -out server.csr -sha256 -subj "/CN=server on my home"

# Sign the server csr
openssl x509 -req -days 365 -in server.csr -sha256 -CA root-ca.crt -CAkey root-ca.pem -CAcreateserial -out server.crt

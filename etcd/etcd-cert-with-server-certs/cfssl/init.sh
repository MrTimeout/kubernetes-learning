#!/bin/bash
# First parameter is optional and specifies if it has to print optional logs

CLIENT_PORT=2379
is_debug_mode=0
if [[ ${1:-0} -ne 0 ]]; then
  is_debug_mode=1
fi

# We generate the private key and CSR for the CA
cfssl gencert -initca ./ca.csr.json | cfssljson -bare ca -

if [[ $is_debug_mode -ne 0 ]]; then
  # We verify the CA CSR
  openssl req -text -noout -verify -in ca.csr

  # We verify the private key of CA
  openssl ec -in ca-key.pem -check

  # We verify the certificate, which was built up from the private key and the CSR
  openssl x509 -text -noout -in ca.pem
fi

cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca.conf.json -profile=server server.csr.json | cfssljson -bare server -

if [[ $is_debug_mode -ne 0 ]]; then
  # Checking of the CSR, private key and certificate.
  openssl req -text -noout -verify -in server.csr

  openssl ec -in server-key.pem -check

  openssl x509 -text -noout -in server.pem
fi

docker image build --no-cache --build-arg ALPINE_VERSION=3.16 --build-arg ETCD_VERSION=3.5.4 --tag myetcd:latest --tag myetcd:0.1-SNAPSHOT -f- . <<EOF
ARG ALPINE_VERSION=3.16
FROM alpine:\${ALPINE_VERSION}

ARG ETCD_VERSION

RUN wget https://github.com/etcd-io/etcd/releases/download/v\${ETCD_VERSION}/etcd-v\${ETCD_VERSION}-linux-amd64.tar.gz -O/tmp/etcd.tar.gz && \
  tar -xvzf /tmp/etcd.tar.gz -C /tmp && rm /tmp/etcd.tar.gz && mv /tmp/*/etcd* /usr/local/bin/

COPY ./server-key.pem /server-key.pem
COPY ./server.pem /server.crt

EXPOSE ${CLIENT_PORT}
EOF

mkdir --parent /tmp/etcd/data

docker container run --name etcd-cert-with-server-certs --detach \
  --publish ${CLIENT_PORT}:${CLIENT_PORT} \
  --mount type=bind,source=/tmp/etcd/data,destination=/etcd/data myetcd:0.1-SNAPSHOT \
  /usr/local/bin/etcd \
  --name etcd \
  --data-dir /etcd/data \
  --cert-file /server.crt \
  --key-file /server-key.pem \
  --advertise-client-urls https://0.0.0.0:${CLIENT_PORT} \
  --listen-client-urls https://0.0.0.0:${CLIENT_PORT} \
  --log-level info \
  --log-outputs stderr \
  --logger zap

docker image build --file ../../Dockerfile --tag etcdctl:0.1-SNAPSHOT --tag etcdctl:latest .

docker container run --name etcdctl-client -it \
  --env CLIENT_PORT=${CLIENT_PORT} --env TARGET_IP=172.17.0.2 \
  --mount type=bind,source=$PWD/ca.pem,destination=/ca.pem \
  --add-host etcd:172.17.0.2 \
  etcdctl:0.1-SNAPSHOT '
  /usr/local/bin/etcdctl --endpoints=https://${TARGET_IP}:${CLIENT_PORT} --cacert=/ca.pem put foo bar && \
  /usr/local/bin/etcdctl --endpoints=https://${TARGET_IP}:${CLIENT_PORT} --cacert=/ca.pem get foo --write-out=json | jq .'

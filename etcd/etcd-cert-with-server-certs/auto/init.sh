#!/bin/bash

CLIENT_PORT=2379

docker image build --no-cache --build-arg ALPINE_VERSION=3.16 --build-arg ETCD_VERSION=3.5.4 --tag myetcd:latest --tag myetcd:0.1-SNAPSHOT -f- . <<EOF
ARG ALPINE_VERSION=3.16
FROM alpine:\${ALPINE_VERSION}

ARG ETCD_VERSION

RUN wget https://github.com/etcd-io/etcd/releases/download/v\${ETCD_VERSION}/etcd-v\${ETCD_VERSION}-linux-amd64.tar.gz -O/tmp/etcd.tar.gz && \
  tar -xvzf /tmp/etcd.tar.gz -C /tmp && rm /tmp/etcd.tar.gz && mv /tmp/*/etcd* /usr/local/bin/

EXPOSE ${CLIENT_PORT}
EOF

docker container run --name etcd-cert-with-server-certs --detach \
  --publish ${CLIENT_PORT}:${CLIENT_PORT} \
  --mount type=bind,source=/tmp/etcd/data,destination=/etcd/data myetcd:0.1-SNAPSHOT \
  /usr/local/bin/etcd \
  --name etcd \
  --data-dir /etcd/data \
  --auto-tls \
  --advertise-client-urls https://0.0.0.0:${CLIENT_PORT} \
  --listen-client-urls https://0.0.0.0:${CLIENT_PORT} \
  --log-level info \
  --log-outputs stderr \
  --logger zap

echo "curl https://127.0.0.1:${CLIENT_PORT}/v2/keys/foo -XPUT -d value=bar -v"

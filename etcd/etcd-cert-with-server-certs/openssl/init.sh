#!/bin/bash
# DISCLAIMER: Not already working...

CLIENT_PORT=2379

# Generate the private key for the root
openssl genpkey -algorithm RSA -out ca-root.pem -pkeyopt rsa_keygen_bits:4096 -pkeyopt rsa_keygen_pubexp:65537 -quiet

# Generate the CSR (Certificate Signing Request)
openssl req -new -key ca-root.pem -out ca-root.csr -sha256 -subj "/CN=etcd ca"

# Create the certificate from the private key and CSR. How to display info about it: openssl x509 -in ./ca-root.crt -text -noout
openssl x509 -req -days 365 -in ca-root.csr -signkey ca-root.pem -sha256 -out ca-root.crt -extfile ./ca-root.cnf -extensions ca-root

# Generate the private key for server etcd
openssl genpkey -algorithm RSA -out server.pem -pkeyopt rsa_keygen_bits:4096 -pkeyopt rsa_keygen_pubexp:65537 -quiet

# Generate the CSR for the server etcd
openssl req -new -key server.pem -out server.csr -sha256 -subj "/CN=etcd server"

# Create the certificate
openssl x509 -req -days 365 -in server.csr -sha256 -CA ca-root.crt -CAkey ca-root.pem -CAcreateserial -out server.crt -extfile ./server.cnf -extensions server

# ALPINE_VERSION=3.16 ETCD_VERSION=3.5.4
docker image build --no-cache --build-arg ALPINE_VERSION=3.16 --build-arg ETCD_VERSION=3.5.4 --tag myetcd:latest --tag myetcd:0.1-SNAPSHOT -f- . <<EOF
ARG ALPINE_VERSION=3.16
FROM alpine:\${ALPINE_VERSION}

ARG ETCD_VERSION

RUN wget https://github.com/etcd-io/etcd/releases/download/v\${ETCD_VERSION}/etcd-v\${ETCD_VERSION}-linux-amd64.tar.gz -O/tmp/etcd.tar.gz && \
  tar -xvzf /tmp/etcd.tar.gz -C /tmp && rm /tmp/etcd.tar.gz && mv /tmp/*/etcd* /usr/local/bin/

COPY ./server.pem /server.pem
COPY ./server.crt /server.crt

EXPOSE ${CLIENT_PORT}
EOF

mkdir --parent /tmp/etcd/data

docker container run --name etcd-cert-with-server-certs --detach \
  --publish ${CLIENT_PORT}:${CLIENT_PORT} \
  --mount type=bind,source=/tmp/etcd/data,destination=/etcd/data myetcd:0.1-SNAPSHOT \
  /usr/local/bin/etcd \
  --name etcd \
  --data-dir /etcd/data \
  --cert-file /server.crt \
  --key-file /server.pem \
  --advertise-client-urls https://0.0.0.0:${CLIENT_PORT} \
  --listen-client-urls https://0.0.0.0:${CLIENT_PORT} \
  --log-level info \
  --log-outputs stderr \
  --logger zap

docker image build --file ../../Dockerfile --tag etcdctl:0.1-SNAPSHOT --tag etcdctl:latest .

docker container run --name etcdctl-client -it \
  --env CLIENT_PORT=${CLIENT_PORT} --env TARGET_IP=172.17.0.2 \
  --mount type=bind,source=$PWD/ca-root.crt,destination=/ca-root.crt \
  --add-host etcd:172.17.0.2 \
  etcdctl:0.1-SNAPSHOT '
  /usr/local/bin/etcdctl --endpoints=https://${TARGET_IP}:${CLIENT_PORT} --cacert=/ca-root.crt put foo bar && \
  /usr/local/bin/etcdctl --endpoints=https://${TARGET_IP}:${CLIENT_PORT} --cacert=/ca-root.crt get foo --write-out=json | jq .'

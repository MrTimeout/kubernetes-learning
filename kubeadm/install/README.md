# Help!

## Kubeadm Port requirements

### Control plane node

- Kubernetes api server: 6443
- etcd server client API: 2379-2380
- Kubelet API: 10250
- Kube-scheduler: 10251
- Kube-controller-manager: 10252

### Worker node

- Kubelet API: 10250
- NodePort services: 30000-32767

#!/bin/bash

ip_address=$(ip address show enp0s8 | grep inet | head -n 1 | awk '{print $2}' | cut -d'/' -f1)

sudo kubeadm init --apiserver-advertise-address=$ip_address \
  --apiserver-cert-extra-sans=$ip_address \
  --pod-network-cidr=192.168.1.0/24 \
  --node-name $(hostname -s)
  # --ignore-preflight-errors Swap

mkdir -p $HOME/.kube && \
  cp -i /etc/kubernetes/admin.conf $HOME/.kube/config && \
  chown $(id -u):$(id -g) $HOME/.kube/config

# We need to install a CRI network plugin to work with CoreDNS for instance.
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

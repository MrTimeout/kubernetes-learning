#!/bin/bash
# This script must be executed after vagrant initialization

vagrant ssh master --command "sudo cat /etc/kubernetes/admin.conf" > ~/.kube/config

kubectl version

join=$(vagrant ssh master --command "sudo kubeadm token create --print-join-command")

token=$(sed 's/.*token \(.*\) -.*/\1/g' <<< $join)
cert_hash=$(sed 's/.*hash \(.*\)/\1/g' <<< $join)

echo "Entering the kubernetes master using the command $join"

# Actually not working, we have to join each node separately and manually...
#ssh -i .vagrant/machines/worker0/virtualbox/private_key vagrant@192.168.56.3 \
#  /bin/bash -c "kubeadm_join"

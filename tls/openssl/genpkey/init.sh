#!/bin/bash

# Links:
# - https://www.mkssoftware.com/docs/man1/openssl_genpkey.1.asp
# - https://avinetworks.com/glossary/elliptic-curve-cryptography/
# - https://gist.github.com/briansmith/2ee42439923d8e65a266994d0f70180b

# By default the format is PEM (PEM|DER)
# Algorithms allowed are: RSA, RSA-PSS, EC, X25519, X448, ED25519, ED448
openssl genpkey -algorithm RSA -out key.pem

# We can use the quiet option to not display the dots
openssl genpkey -algorithm RSA -out key.pem -quiet

# It will create a private key encrypted with the password hello and the AES 128B
# The output file will start with "BEGIN ENCRYPTED PRIVATE KEY"
openssl genpkey -algorithm RSA -out key.pem -aes-128-cbc -pass pass:hello

# Elliptic Curve Cryptographic (ECC)
# ec_paramgen_curve:
#   - 160
#   - 224
#   - 256
#   - 384
#   - 521
# ec_param_enc: (This is the actual encoding used for parameters)
#   - named_curve (default)
#   - explicit
openssl genpkey -algorihtm EC -pkeyopt ec_paramgen_curve:P-256 -pkeyopt ec_param_enc:named_curve -out key.pem -quiet

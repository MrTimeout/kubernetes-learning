#!/bin/bash
#
# Main arguments is the alpine version. By default is 3.16 because is the more recent. Feel free to change it.
ALPINE_VERSION=${1:-3.16}

docker image build --build-arg ALPINE_VERSION=$ALPINE_VERSION --tag alpinebash:${ALPINE_VERSION} --tag alpinebash:latest -<<EOF
FROM alpine:$ALPINE_VERSION

RUN apk add --no-cache --update bash && sed -i 's/\(.*\)\/bin\/ash/\1\/bin\/bash/g' /etc/passwd

ENTRYPOINT ["/bin/bash"]
EOF
